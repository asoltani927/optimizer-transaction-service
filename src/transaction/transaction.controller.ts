import { Controller } from '@nestjs/common';
import {MessagePattern, Payload} from "@nestjs/microservices";
import {ChartFluctuationQueryDto} from "./dto/ChartFluctuationQuery.dto";
import {TransactionService} from "./transaction.service";

@Controller('transaction')
export class TransactionController {

    constructor(private transactionService: TransactionService) {
    }

    @MessagePattern('get_fluctuation_transaction_chart')
    async get_fluctuation_transaction_chart(
        @Payload() data: ChartFluctuationQueryDto,
    ) {
        return this.transactionService.getFluctuationOfTransaction(
            data.slug,
            data.overtime,
        );
    }

}
