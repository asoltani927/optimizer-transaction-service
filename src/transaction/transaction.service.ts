import {Inject, Injectable} from '@nestjs/common';
import {ClientProxy} from '@nestjs/microservices';
import {LineChartDTO} from "./dto/LineChartDTO";
import {DateTool} from "./tools/date.tool";
import {TransactionRepository} from "./repository/Transaction.repository";
import {Overtime} from "./transaction.enum";
import {CollectionService} from "../collection/collection.service";

@Injectable()
export class TransactionService {

    constructor(@Inject('SERVICES') private readonly services: ClientProxy,
                private readonly transactionRepository: TransactionRepository,
                private readonly collectionService: CollectionService,
    ) {
    }

    async detectCollections(slug: string, name: string) {
        return this.services.send('detect_collections', {slug: slug, name: name});
    }

    async getFluctuationOfTransaction(slug: string, overtime: Overtime) {
        switch (overtime) {
            case Overtime.weekly:
                return this.arrangeData(
                    await this.transactionRepository.getFluctuationDaily(slug, 7),
                    true,
                );

            case Overtime.monthly:
                return this.arrangeData(
                    await this.transactionRepository.getFluctuationDaily(slug, 30),
                    true,
                );

            case Overtime.sixMonth:
                return this.arrangeData(
                    await this.transactionRepository.getFluctuationMonthly(slug, 6),
                );
        }
    }

    async arrangeData(data, daily = false): Promise<LineChartDTO> {

        if (data.length > 0) {
            let collection = await this.collectionService.getCollectionBySlug(data[0].slug);
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                name: collection.name,
                values: data.map((item: any) => {
                    try {
                        console.log(item)
                        return parseFloat(item.average).toFixed(3);
                    } catch (e) {
                        return 0;
                    }
                }),
                labels: data.map((item: any) => {
                    if (daily)
                        return (
                            DateTool.getMonthShortName(parseInt(item.month)).toString() +
                            '-' +
                            item.day
                        );
                    return DateTool.getMonthShortName(parseInt(item.month)).toString();
                }),
            };
        }
        return {
            name: '',
            values: [],
            labels: [],
        };
    }
}
