import {Injectable} from "@nestjs/common";
import {Cron} from "@nestjs/schedule";
import {OpenSeaTool} from "../tools/opensea.tool";
import {EventType} from "../transaction.enum";
import {DelayTool} from "../tools/delay.tool";
import {Web3Tool} from "../tools/web3.tool";
import {EventCreateDTO} from "../dto/EventCreate.dto";
import {EventRepository} from "../repository/Event.repository";
import {TransactionService} from "../transaction.service";
import {InjectQueue} from "@nestjs/bull";
import {Queue} from "bull";

// sales
@Injectable()
export class SuccessfulEventsHourlySchedule {

    constructor(private eventRepository: EventRepository,
                private service: TransactionService,
                @InjectQueue('event-queue') private queue: Queue,
    ) {
    }

    @Cron('30 0 * * * *', {
        name: 'successful_event_hourly',
        // timeZone: process.env.TIMEZONE
    })
    async trigger() {
        let page = 0;
        let occurred_before = new Date();
        let occurred_after = new Date();
        occurred_after.setHours(occurred_after.getHours() - 1);
        await this.queue.add('collect-job', {
            type: EventType.successful,
            occurred_after: occurred_after,
            occurred_before: occurred_before,
            limit: 50,
            page: (page),
        });
    }
}
