import {Injectable} from '@nestjs/common';
import {Cron} from '@nestjs/schedule';
import {EventRepository} from "../repository/Event.repository";
import {EventType} from "../transaction.enum";
import {PriceChangesDto} from "../dto/PriceChanges.dto";
import {PriceChangesRepository} from "../repository/PriceChanges.repository";

@Injectable()
export class PriceChangesTwoDaysAverageSchedule {


    constructor(private eventRepository: EventRepository, private priceChangesRepository: PriceChangesRepository) {

    }

    @Cron('0 0 0 * * *', {
        name: 'prices_changes_two_days_average',
        // timeZone: process.env.TIMEZONE,
    })
    async trigger() {
        let data = await this.eventRepository.getTwoDaysAgoEvents(EventType.created);
        data.forEach((item: any, index: any) => {
            const diffTime = Math.abs(item.ended_at - item.started_at);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            let data: PriceChangesDto = {
                name: item._id.name,
                slug: item._id.slug,
                type: item._id.type,
                token_id: item._id.token_id,
                asset_name: item._id.asset_name,
                average: item.unit_price_avg,
                started_at: item.started_at,
                ended_at: item.ended_at,
                during_days: diffDays,
                data_count: item.count,
            }
            this.priceChangesRepository.create(data);
        });
    }
}
