import {Injectable} from '@nestjs/common';
import {Cron} from '@nestjs/schedule';
import {InjectQueue} from "@nestjs/bull";
import {Queue} from "bull";
import {CollectionService} from "../../collection/collection.service";
import {EtherContractsLogsJobName, EtherContractsLogsQueueName} from "../jobs/EtherContractsLogsScanner.job";

@Injectable()
export class EtherTransactionHistoryScannerSchedule {

    constructor(
        private collectionService: CollectionService,
        @InjectQueue(EtherContractsLogsQueueName) private queue: Queue,
    ) {
    }

    @Cron('0 45 21 * * *', {
        name: 'ether_scan_transaction_schedule',
    })
    async trigger() {
        const contractsList = await this.collectionService.getCollectionUnRecovered();
        let contracts = [];
        for (const contract of contractsList) {
            contracts.push({
                slug: contract.slug,
                address: contract.contract_address,
                topics: contract.topics,
            })
            if (contracts.length === 20) {
                await this.pushQueue(contracts)
                contracts = [];
            }
        }
        if (contracts.length > 0) {
            await this.pushQueue(contracts)
        }
    }

    async pushQueue(contracts) {
        return this.queue.add(EtherContractsLogsJobName, {
            contracts
        })
    }
}
