import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { EventRepository } from '../repository/Event.repository';
import { TransactionAverageCreateDTO } from '../dto/TransactionAverageCreate.dto';
import { EventType } from '../transaction.enum';
import { TransactionAveragesRepository } from '../repository/TransactionAverages.repository';
import { TransactionRepository } from '../repository/Transaction.repository';

@Injectable()
export class TransactionTwoDaysAverageSchedule {
  constructor(
    private transactionRepository: TransactionRepository,
    private transactionAveragesRepository: TransactionAveragesRepository,
  ) {}

  @Cron('40 0 0 * * *', {
    name: 'transactions_two_days_average',
    // timeZone: process.env.TIMEZONE,
  })
  async trigger() {
    const data = await this.transactionRepository.getTwoDaysAgoAverages(
      EventType.created,
    );
    data.forEach((item: any, index: any) => {
      const diffTime = Math.abs(item.ended_at - item.started_at);
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
      const data: TransactionAverageCreateDTO = {
        name: item._id.name,
        slug: item._id.slug,
        type: item._id.type,
        average: item.avg,
        started_at: item.started_at,
        ended_at: item.ended_at,
        during_days: diffDays,
        data_count: item.count,
        transaction_count: item.transaction_count,
      };
      this.transactionAveragesRepository.create(data);
    });
  }
}
