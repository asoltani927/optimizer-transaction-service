import {Injectable} from '@nestjs/common';
import {Cron} from '@nestjs/schedule';
import {EventRepository} from "../repository/Event.repository";
import {TransactionService} from "../transaction.service";
import {TransactionRepository} from "../repository/Transaction.repository";

@Injectable()
export class CalculateTransactionsCollectionsDailySchedule {

    constructor(private eventRepository: EventRepository, private transactionRepository: TransactionRepository, private service: TransactionService) {
    }

    @Cron('0 4 */1 * *', {
        name: 'calculate_transactions_daily_collection',
        // timeZone: process.env.TIMEZONE,
    })
    async trigger() {

        // let events = await this.eventRepository.getTodaySuccessfulEvent();
        // if (events) {
        //     events.forEach((item: any) => {
        //         let data  : TransactionCreateDTO = {
        //             name: item.name,
        //             slug: item.slug,
        //             type: item.type,
        //             tag: item._id.tag,
        //             count: item.count,
        //             avg: item.average,
        //         }
        //         this.transactionRepository.create(data);
        //     });
        // }
    }
}
