import {Injectable} from '@nestjs/common';
import {Cron} from '@nestjs/schedule';
import {EventRepository} from "../repository/Event.repository";
import {TransactionService} from "../transaction.service";
import {EventType} from "../transaction.enum";
import {InjectQueue} from "@nestjs/bull";
import {Queue} from "bull";

@Injectable()
export class TransferEventsHourlySchedule {

    constructor(
        private eventRepository: EventRepository,
        private service: TransactionService,
        @InjectQueue('event-queue') private queue: Queue,
    ) {
    }

    @Cron('45 0 * * * *', {
        name: 'transfer_events_collector',
        // timeZone: process.env.TIMEZONE,
    })
    async trigger() {

        let page = 0;
        let occurred_before = new Date();
        let occurred_after = new Date();
        occurred_after.setHours(occurred_after.getHours() - 1);
        await this.queue.add('collect-job', {
            type: EventType.transfer,
            occurred_after: occurred_after,
            occurred_before: occurred_before,
            limit: 50,
            page: (page),
        });
    }
}
