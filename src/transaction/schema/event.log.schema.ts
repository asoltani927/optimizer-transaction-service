import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type EventLogDocument = EventLog & Document;

@Schema()
export class EventLog {

    @Prop({ required: true })
    block_number: string;

    @Prop({ required: true })
    address: Array<string>;

    @Prop({ required: true })
    topics: Array<string>;

    @Prop({ required: true })
    content: Array<any>;

    @Prop({ default: Date.now })
    created_at: Date;

    @Prop({ default: Date.now })
    updated_at: Date;
}

export const EventLogSchema = SchemaFactory.createForClass(EventLog);
