import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TransactionDocument = Transaction & Document;

@Schema()
export class Transaction {

  @Prop({ required: true })
  slug: string;

  @Prop({ required: true })
  contract_address: string;

  @Prop({ required: true })
  blockHash: string;

  @Prop({ required: true })
  blockNumber: string;

  @Prop({ required: true })
  gasUsed: string;

  @Prop({ required: true })
  cumulativeGasUsed: string;

  @Prop({ required: true })
  effectiveGasPrice: string;

  @Prop({ required: true })
  from: string;

  @Prop({ required: true })
  logs: Array<any>;

  @Prop({ required: true })
  logsBloom: string;

  @Prop()
  to: string;

  @Prop({ required: true })
  transactionIndex: string;

  @Prop()
  maxFeePerGas: string;

  @Prop()
  maxPriorityFeePerGas: string;

  @Prop()
  hash: string;

  @Prop()
  nonce: string;

  @Prop()
  accessList: Array<any>;

  @Prop()
  chainId: string;

  @Prop()
  value: string;

  @Prop()
  cost: number;

  @Prop()
  input: string;

  @Prop()
  status: string;

  @Prop()
  type: string;

  @Prop()
  timestamp: string;

  @Prop({default: null})
  created_time: Date;

  @Prop({ default: Date.now })
  created_at: Date;

  @Prop({ default: Date.now })
  updated_at: Date;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
