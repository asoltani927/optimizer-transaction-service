import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose"
import {Document} from "mongoose"
import {Factory} from "nestjs-seeder";
import {StrTool} from "../tools/str.tool";

export type EventDocument = Event & Document

@Schema()
export class Event {
    @Factory((factor, ctx) => {
        return ctx.slug
    })
    @Prop({required: true})
    name: string

    @Factory((factor, ctx) => {
        return ctx.slug
    })
    @Prop({required: true})
    slug: string

    @Factory((factor, ctx) => {
        return Math.round(Math.random() * (99999 - 10000) + 10000);
    })
    @Prop()
    token_id: string

    @Factory((factor, ctx) => {
        return ctx.slug + "#" + (Math.round(Math.random() * (99999 - 10000) + 10000))
    })
    @Prop()
    asset_name: string

    @Factory((factor, ctx) => {
        return ctx.type
    })
    @Prop({required: true})
    type: string

    @Factory((factor, ctx) => {
        return Math.random();
    })
    @Prop({required: true})
    unit_price: number

    @Factory((factor, ctx) => {
        return 1
    })
    @Prop({required: true})
    quantity: number

    @Factory((factor, ctx) => {
        return StrTool.random(25);
    })
    @Prop()
    from: string

    @Factory((factor, ctx) => {
        return StrTool.random(25);
    })
    @Prop()
    to: string

    @Factory((factor, ctx) => {
        return ctx.slug.toString() + "-" + ctx.date.toISOString().substring(0, 10)
    })
    @Prop({required: true})
    tag: string

    @Factory((factor, ctx) => {
        return ctx.date.getFullYear() + '-' + ctx.date.getMonth()
    })
    @Prop()
    month: string

    @Factory((factor, ctx) => {
        return ctx.date.getTime()
    })
    @Prop()
    action_date: Date

    @Factory((factor, ctx) => {
        return ctx.date
    })
    @Prop({default: Date.now})
    created_at: Date

    @Factory((factor, ctx) => {
        return ctx.date
    })
    @Prop({default: Date.now})
    updated_at: Date
}

export const EventSchema = SchemaFactory.createForClass(Event);
