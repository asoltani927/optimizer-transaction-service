import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import {Factory} from "nestjs-seeder";

export type PricesChangeDocument = PricesChange & Document;

@Schema()
export class PricesChange {
  @Factory((factor, ctx) => {
    return ctx.slug
  })
  @Prop({required: true})
  name: string

  @Factory((factor, ctx) => {
    return ctx.slug
  })
  @Prop({required: true})
  slug: string

  @Factory((factor, ctx) => {
    return (Math.round(Math.random() * (99999 - 10000) + 10000)).toString();
  })
  @Prop({ required: true })
  token_id: string;

  @Factory((factor, ctx) => {
    return ctx.slug + "#" + (Math.round(Math.random() * (99999 - 10000) + 10000))
  })
  @Prop({ required: true })
  asset_name: string;

  @Factory((factor, ctx) => {
    return ctx.type
  })
  @Prop({ required: true })
  type: string;

  @Factory((factor, ctx) => {
    return Math.random();
  })
  @Prop({ required: true })
  average: number;

  @Factory((factor,ctx) => {
    return ctx.start;
  })
  @Prop({ required: true })
  started_at: Date;

  @Factory((factor,ctx) => {
    return ctx.end;
  })
  @Prop({ required: true })
  ended_at: Date;

  @Factory((factor,ctx) => {
    return 2;
  })
  @Prop({ required: true })
  during_days: number;

  @Factory((factor,ctx) => {
    return 2;
  })
  @Prop({ required: true })
  data_count: number;

  @Factory((factor,ctx) => {
    return ctx.date;
  })
  @Prop({ default: Date.now })
  created_at: Date;

  @Factory((factor,ctx) => {
    return ctx.date;
  })
  @Prop({ default: Date.now })
  updated_at: Date;
}

export const PricesChangeSchema = SchemaFactory.createForClass(PricesChange);
