import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document} from 'mongoose';
import {Factory} from "nestjs-seeder";

export type TransactionAveragesDocument = TransactionAverages & Document;

@Schema()
export class TransactionAverages {

    @Factory((factor, ctx) => {
        return ctx.slug
    })
    @Prop({required: true})
    name: string;

    @Factory((factor, ctx) => {
        return ctx.slug
    })
    @Prop({required: true})
    slug: string;

    @Factory((factor, ctx) => {
        return ctx.event
    })
    @Prop({required: true})
    type: string;

    @Factory(() => {
        return Math.random();
    })
    @Prop({required: true})
    average: number;

    @Factory((factor,ctx) => {
        return ctx.start;
    })
    @Prop({required: true})
    started_at: Date;

    @Factory((factor,ctx) => {
        return ctx.end;
    })
    @Prop({required: true})
    ended_at: Date;

    @Factory((factor,ctx) => {
        return 2;
    })
    @Prop({required: true})
    during_days: number;

    @Factory((factor,ctx) => {
        return 2;
    })
    @Prop({required: true})
    data_count: number;

    @Factory((factor,ctx) => {
        return 2;
    })
    @Prop({required: true})
    transaction_count: number;

    @Factory((factor,ctx) => {
        return ctx.date;
    })
    @Prop({default: Date.now})
    created_at: Date;

    @Factory((factor,ctx) => {
        return ctx.date;
    })
    @Prop({default: Date.now})
    updated_at: Date;
}

export const TransactionAveragesSchema =
    SchemaFactory.createForClass(TransactionAverages);
