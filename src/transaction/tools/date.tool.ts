export class DateTool{
    static dateWithMonthsDelay(months: any) {
        const date = new Date();
        date.setMonth(date.getMonth() + months);

        return date;
    }

    static subWeeks(weeks: Number = 1) {
        const date = new Date();
        // @ts-ignore
        date.setDate(date.getDate() - ((weeks - 1) * 7));
        return date;
    }

    static subDays(days: Number = 1) {
        const date = new Date();
        // @ts-ignore
        date.setDate(date.getDate() - (days -  1));
        return date;
    }

    static subMonths(months: Number = 1) {
        const date = new Date();
        // @ts-ignore
        date.setDate(date.getDate() - ((months - 1) * 30));
        return date;
    }

    static differenceMonths(d1: Date, d2: Date) {
        let months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    static getMonthShortName(num: Number) {
        const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        // @ts-ignore
        if (months[num - 1] !== undefined) {
            // @ts-ignore
            return months[num - 1];
        }
        return null;
    }
}
