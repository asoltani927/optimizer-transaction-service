import {Api} from "src/contracts/Api";
import {EventOpenseaSearchDTO} from "../dto/EventOpenseaSearch.dto";
import {EventType} from "../transaction.enum";

export class OpenSeaApi extends Api {

    public async getEvents(filter: EventOpenseaSearchDTO, limit: Number = 300, offset: Number = 0) {
        this.setAction("/events");
        if (filter.slug !== null) {
            this.addParam("collection_slug", filter.slug);
        }
        if (filter.type !== null) {
            this.addParam("event_type", filter.type);
        }
        if (filter.occurred_after !== null) {
            this.addParam("occurred_after", filter.occurred_after);
        }
        if (filter.occurred_before !== null) {
            this.addParam("occurred_before", filter.occurred_before);
        }
        this.addParam("limit", limit);
        this.addParam("offset", offset);
        const response = await this.getResponse();
        if (response !== null) {
            // @ts-ignore
            return response;
        }
        return null;
    }

    public async getTransfer(slug: string, occurred_after: Number, occurred_before: Number, limit: Number = 50, offset: Number = 0) {
        let filter: EventOpenseaSearchDTO = {
            slug: slug,
            type: EventType.transfer,
            occurred_after: occurred_after,
            occurred_before: occurred_before
        }
        return await this.getEvents(filter, limit, offset)
    }

    public async getAll(type: EventType, occurred_after: Number, occurred_before: Number, limit: Number = 50, offset: Number = 0) {
        let filter: EventOpenseaSearchDTO = {
            slug: null,
            type: EventType.transfer,
            occurred_after: occurred_after,
            occurred_before: occurred_before
        }
        return await this.getEvents(filter, limit, offset)
    }

    public async getSuccessful(slug: string, occurred_after: Number, occurred_before: Number, limit: Number = 50, offset: Number = 0) {
        let filter: EventOpenseaSearchDTO = {
            slug: slug,
            type: EventType.successful,
            occurred_after: occurred_after,
            occurred_before: occurred_before
        }
        return await this.getEvents(filter, limit, offset)
    }
}

export const OpenSeaTool = new OpenSeaApi("https://api.opensea.io/api/v1");
