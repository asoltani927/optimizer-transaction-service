export class Delay{
    sleep = (ms: Number) => {
        // @ts-ignore
        return new Promise((resolve) => setTimeout(resolve, ms));
    };
}

export const DelayTool = new Delay();