
export class Web3Service{
    protected instance

    protected provider: any

    constructor() {
        const Web3 = require("web3");
        this.provider = new Web3.providers.HttpProvider(process.env.WEB3_WSS_HOST)
        this.instance = new Web3(this.provider);
    }

    getEth(){
        return this.instance.eth
    }

    getUtils(){
        return this.instance.utils
    }
}

export const Web3Tool = new Web3Service();
