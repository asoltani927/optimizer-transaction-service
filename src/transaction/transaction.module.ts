import {Module} from '@nestjs/common';
import {TransactionService} from './transaction.service';
import {TransactionController} from './transaction.controller';
import {MongooseModule} from '@nestjs/mongoose';
import {Event, EventSchema} from './schema/event.schema';
import {EventRepository} from './repository/Event.repository';
import {ClientsModule, Transport} from '@nestjs/microservices';
import {Transaction, TransactionSchema} from "./schema/transaction.schema";
import {TransactionRepository} from "./repository/Transaction.repository";
import {BullModule} from "@nestjs/bull";
import {CollectionModule} from "../collection/collection.module";
import {EtherTransactionHistoryScannerSchedule} from "./schedule/EtherTransactionHistoryScanner.schedule";
import {EtherScanModule} from "../etherScan/etherScan.module";
import {EventLog, EventLogSchema} from "./schema/event.log.schema";
import {EventLogRepository} from "./repository/EventLog.repository";
import {EtherContractsLogsQueueName, EtherContractsLogsScannerJob} from "./jobs/EtherContractsLogsScanner.job";
import {
    EtherTransactionsFromLogsScannerJob,
    EtherTransactionsQueueName,
} from "./jobs/EtherTransactionsFromLogsScanner.job";

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: EventLog.name,
                schema: EventLogSchema,
            },
            {
                name: Transaction.name,
                schema: TransactionSchema,
            },

            //
            // {
            //     name: Event.name,
            //     schema: EventSchema,
            // },
            // {
            //     name: TransactionAverages.name,
            //     schema: TransactionAveragesSchema,
            // },
            // {
            //     name: PricesChange.name,
            //     schema: PricesChangeSchema,
            // },
        ]),
        ClientsModule.register([
            {
                name: 'SERVICES',
                transport: Transport.REDIS,
                options: {
                    url: process.env.REDIS_URL,
                },
            },
        ]),
        BullModule.registerQueue({
            name: EtherContractsLogsQueueName,
            limiter: {
                max: 5,
                duration: 1000
            }
        }),
        BullModule.registerQueue({
            name: EtherTransactionsQueueName,
            limiter: {
                max: 5,
                duration: 1000
            }
        }),
        CollectionModule,
        EtherScanModule
    ],
    providers: [
        TransactionService,
        // EventRepository,
        // TransactionAveragesRepository,
        // PriceChangesRepository,
        //
        // CreatedEventsHourlySchedule,
        // SuccessfulEventsHourlySchedule,
        // TransferEventsHourlySchedule,
        // CalculateTransactionsCollectionsDailySchedule,
        // PriceChangesTwoDaysAverageSchedule,
        // TransactionTwoDaysAverageSchedule,
        //
        // EventJob
        //
        EventLogRepository,
        TransactionRepository,


        EtherTransactionHistoryScannerSchedule,

        EtherTransactionsFromLogsScannerJob,
        EtherContractsLogsScannerJob
    ],
    controllers: [
        TransactionController
    ],
})
export class TransactionModule {
}
