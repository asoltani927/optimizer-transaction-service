import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {TransactionCreateDTO} from '../dto/TransactionCreate.dto';
import {Transaction, TransactionDocument} from '../schema/transaction.schema';
import {EventType} from '../transaction.enum';
import {DateTool} from "../tools/date.tool";

@Injectable()
export class TransactionRepository {
    constructor(
        @InjectModel(Transaction.name)
        private model: Model<TransactionDocument>,
    ) {
    }

    async create(
        dto: TransactionCreateDTO,
    ): Promise<Transaction> {
        const model = new this.model(dto);
        return await model.save();
    }

    async getTwoDaysAgoAverages(type: EventType) {
        const start = new Date();
        start.setDate(start.getDate() - 2);
        start.setHours(0);
        start.setMinutes(0);
        start.setSeconds(0);
        const to = new Date();
        to.setHours(0);
        to.setMinutes(0);
        to.setSeconds(0);
        return this.getAgoAveragesByDate(type, start, to)
    }

    async getAgoAveragesByDate(type: EventType, from, to) {
        return this.model.aggregate([
            {
                $match: {
                    created_time: {$ne: null}
                }
            },
            {
                $match: {
                    created_time: {
                        $gte: from,
                        $lte: to,
                    },
                    type: type,
                },
            },
            {
                $group: {
                    _id: {
                        slug: '$slug',
                        type: '$type',
                    },
                    started_at: {
                        $first: '$created_time',
                    },
                    ended_at: {
                        $last: '$created_time',
                    },
                    avg: {
                        $cost: '$cost',
                    },
                    transaction_count: {
                        $sum: '$count',
                    },
                    count: {
                        $sum: 1,
                    },
                },
            },
        ]);
    }

    async getFluctuationMonthly(slug: string, numberOfMonth = 7) {
        return await this.getAggregate([
            {$sort: {created_time: -1}},
            {
                $match: {
                    created_time: {$ne: null}
                }
            },
            {
                $match: {
                    slug: {
                        $eq: slug,
                    },
                    created_time: {
                        $gte: DateTool.subMonths(numberOfMonth),
                    },
                }
            },
            {
                $group: {
                    _id: {
                        date: {$dateToString: {format: "%Y-%m", date: "$created_time"}},
                        slug: "$slug",
                    },
                    average: {$avg: "$cost"},
                    count: {$sum: "$count"},
                    month: {$first: {$month: "$created_time"}},
                    slug: {$first: "$slug"},
                },
                //
            },
            {$sort: {_id: 1}},
        ]);
    }

    async getFluctuationDaily(slug: string, numberOfDays = 7) {
        return await this.getAggregate([
            {
                $sort: {created_time: -1}
            },
            {
                $match: {
                    created_time: {$ne: null}
                }
            },
            {
                $match: {
                    slug: {
                        $eq: slug,
                    },
                    created_time: {
                        $gte: DateTool.subDays(numberOfDays),
                    },
                }
            },
            {
                $group: {
                    _id: {
                        date: {$dateToString: {format: "%Y-%m-%d", date: "$created_time"}},
                        slug: "$slug",
                    },
                    // average: {$avg: "$cost"},
                    average: {$avg: "$cost"},
                    month: {$first: {$month: "$created_time"}},
                    day: {$first: {$dayOfMonth: "$created_time"}},
                    slug: {$first: "$slug"},
                },
                //
            },
            {$sort: {_id: 1}},
        ]);
    }


    async getAggregate(aggregate) {
        return this.model.aggregate(aggregate);
    }

    async getByHash(hash) {
        return this.model.findOne({hash});
    }
}
