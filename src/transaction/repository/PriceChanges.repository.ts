import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {PricesChange, PricesChangeDocument} from "../schema/price.change.schema";
import {PriceChangesDto} from "../dto/PriceChanges.dto";

@Injectable()
export class PriceChangesRepository {

    constructor(@InjectModel(PricesChange.name) private priceChangesModel: Model<PricesChangeDocument>) {
    }

    async create(createDTO: PriceChangesDto): Promise<PricesChange> {
        let newChanges = new this.priceChangesModel(createDTO)
        return await newChanges.save();
    }
}
