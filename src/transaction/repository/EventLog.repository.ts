import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {EventLog, EventLogDocument} from "../schema/event.log.schema";
import {EventLogCreateDTO} from "../dto/EventLogCreate.dto";

@Injectable()
export class EventLogRepository {

    constructor(@InjectModel(EventLog.name) private model: Model<EventLogDocument>) {
    }

    async create(dto: EventLogCreateDTO): Promise<EventLog> {
        let model = new this.model(dto)
        return await model.save();
    }
}
