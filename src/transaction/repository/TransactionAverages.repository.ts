import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {TransactionAverageCreateDTO} from "../dto/TransactionAverageCreate.dto";
import {TransactionAveragesDocument, TransactionAverages} from "../schema/transaction.averages.schema";

@Injectable()
export class TransactionAveragesRepository {

    constructor(@InjectModel(TransactionAverages.name) private transactionAveragesModel: Model<TransactionAveragesDocument>) {
    }

    async create(createDTO: TransactionAverageCreateDTO): Promise<TransactionAverages> {
        let newTransaction = new this.transactionAveragesModel(createDTO)
        return await newTransaction.save();
    }
}
