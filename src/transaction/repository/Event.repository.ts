import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {EventCreateDTO} from "../dto/EventCreate.dto";
import {Event, EventDocument} from "../schema/event.schema";
import {EventType} from "../transaction.enum";

@Injectable()
export class EventRepository {

    constructor(@InjectModel(Event.name) private eventModel: Model<EventDocument>) {
    }

    async create(createEventDTO: EventCreateDTO): Promise<Event> {
        let newEvent = new this.eventModel(createEventDTO)
        return await newEvent.save();
    }

    async getTodaySuccessfulEvent() {
        const start = new Date();
        start.setDate(start.getDate() - 1);
        start.setHours(0);
        start.setMinutes(0);
        start.setSeconds(0);
        const to = new Date();
        to.setDate(to.getDate() - 1);
        to.setHours(23);
        to.setMinutes(59);
        to.setSeconds(59);
        return this.eventModel.aggregate([
            {
                $match: {
                    type: {$eq: "successful"},
                    created_at: {$gte: start, $lte: to},
                },
            },
            {
                $group: {
                    _id: {
                        tag: "$tag",
                    },
                    name: {
                        $first: "$name",
                    },
                    slug: {
                        $first: "$slug",
                    },
                    count: {$sum: 1},
                    average: {$avg: "$unit_price"},
                },
            },
        ]);
    }

    async getTwoDaysAgoEvents(type: EventType) {
        const start = new Date();
        start.setDate(start.getDate() - 2);
        start.setHours(0);
        start.setMinutes(0);
        start.setSeconds(0);
        const to = new Date();
        to.setHours(0);
        to.setMinutes(0);
        to.setSeconds(0);
        return this.eventModel.aggregate([
            {
                $match: {
                    created_at: {
                        $gte: start,
                        $lte: to,
                    },
                    type: type,
                }
            },
            {
                $group: {
                    _id: {
                        name: "$name",
                        slug: "$slug",
                        token_id: "$token_id",
                        asset_name: "$asset_name",
                        type: "$type",
                    },
                    started_at: {
                        $first: '$created_at'
                    },
                    ended_at: {
                        $last: '$created_at'
                    },
                    unit_price_avg: {
                        $avg: '$unit_price'
                    },
                    count: {
                        $sum: 1,
                    },
                }
            }
        ]);
    }
}
