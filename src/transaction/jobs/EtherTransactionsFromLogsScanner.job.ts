import {InjectQueue, Process, Processor} from "@nestjs/bull";
import {Job, Queue} from "bull";
import {Logger} from "@nestjs/common";
import {EtherScanService} from "../../etherScan/etherScan.service";
import {TransactionRepository} from "../repository/Transaction.repository";
import {EventLogRepository} from "../repository/EventLog.repository";
import {CollectionRepository} from "../../collection/repository/Collection.repository";
import {Helper} from "../../helpers/helper";

export const EtherTransactionsQueueName = 'ether-transactions-queue';
export const EtherCollectTransactionFromLogsJobName = 'collect-transactions-from-logs-job';

@Processor(EtherTransactionsQueueName)
export class EtherTransactionsFromLogsScannerJob {

    private readonly logger: Logger = new Logger();

    constructor(
        @InjectQueue(EtherTransactionsQueueName) private queue: Queue,
        private etherScanService: EtherScanService,
        private transactionRepository: TransactionRepository,
        private collectionRepository: CollectionRepository,
        private logsRepository: EventLogRepository,
    ) {
    }

    @Process(EtherCollectTransactionFromLogsJobName)
    async readTransactionHistoryScannerJob(job: Job<any>) {
        this.process(job.data.log).then()
    }

    async process(log) {
        try {
            let transaction = await this.etherScanService.getTransactionByHash(log.transactionHash).catch((e) => {
                this.logger.error(e)
            });
            let receipt = await this.etherScanService.getTransactionReceiptByHash(log.transactionHash).catch((e) => {
                this.logger.error(e.status)
            });
            await this.record({
                log,
                transaction,
                receipt,
            })
        } catch (e) {
            this.logger.error(e)
            this.etherScanService.sleep()
            await this.queue.add(EtherCollectTransactionFromLogsJobName, {log})
        }
    }

    async record(data) {
        const {transaction, log, receipt} = data;
        let timestamp = await this.getTimestamp(transaction.blockNumber)
        let contract = await this.collectionRepository.findOneByAddress(log.address)
        await this.logsRepository.create({
            address: log.address,
            content: log.data,
            block_number: log.blockNumber,
            topics: log.topics,
        })
        let duplicatedTransaction = await this.transactionRepository.getByHash(transaction.hash)

        if (!duplicatedTransaction) {
            let created_time = null;
            if (timestamp) {
                created_time = new Date(parseInt(timestamp) * 1000);
            }
            let store = await this.transactionRepository.create({
                accessList: transaction.accessList,
                blockHash: transaction.blockHash,
                blockNumber: transaction.blockNumber,
                chainId: transaction.blockNumber,
                contract_address: log.address,
                cumulativeGasUsed: receipt.cumulativeGasUsed,
                effectiveGasPrice: receipt.effectiveGasPrice,
                from: transaction.from,
                gasUsed: receipt.gasUsed,
                input: transaction.input,
                logs: receipt.logs,
                logsBloom: receipt.logsBloom,
                maxFeePerGas: transaction.maxFeePerGas,
                maxPriorityFeePerGas: transaction.maxPriorityFeePerGas,
                nonce: transaction.nonce,
                slug: contract.slug,
                status: receipt.status,
                timestamp: timestamp,
                created_time: created_time,
                to: transaction.to,
                hash: transaction.hash,
                transactionIndex: transaction.transactionIndex,
                type: ((transaction.type) ? transaction.type : null),
                value: transaction.value,
                cost: await this.etherScanService.parseTransactionValue(transaction.value),
            })
            if (Helper.isDebug()) {
                this.logger.log('------- Transaction Stored ---------- ' + log.transactionHash)
            }
            return store;
        } else {
            if (Helper.isDebug()) {
                this.logger.warn('------- Transaction is duplicated ---------- ' + log.transactionHash)
            }
            return duplicatedTransaction
        }
    }

    async getTimestamp(block) {
        return this.etherScanService.getBlockTimestamp(block)
    }
}
