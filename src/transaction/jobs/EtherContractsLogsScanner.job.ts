import {InjectQueue, Process, Processor} from "@nestjs/bull";
import {Job, Queue} from "bull";
import {Logger} from "@nestjs/common";
import {EtherScanService} from "../../etherScan/etherScan.service";
import {CollectionRepository} from "../../collection/repository/Collection.repository";
import {DelayTool} from "../tools/delay.tool";
import {
    EtherCollectTransactionFromLogsJobName,
    EtherTransactionsQueueName
} from "./EtherTransactionsFromLogsScanner.job";
import {Helper} from "../../helpers/helper";

export const EtherContractsLogsQueueName = 'ether-logs-queue';
export const EtherContractsLogsJobName = 'ether-contracts-logs-scanner-job';

@Processor(EtherContractsLogsQueueName)
export class EtherContractsLogsScannerJob {

    private readonly logger: Logger = new Logger();

    constructor(
        @InjectQueue(EtherContractsLogsQueueName) private queueLogs: Queue,
        @InjectQueue(EtherTransactionsQueueName) private queueTransactions: Queue,
        private etherScanService: EtherScanService,
        private collectionRepository: CollectionRepository,
    ) {
    }

    @Process(EtherContractsLogsJobName)
    async readTransactionHistoryScannerJob(job: Job<any>) {
        let toBlock = await this.etherScanService.getLastBlock()
        let emptyDeepLogsCounter = 0
        let totalOfLogs = [];

        do {
            if (Helper.isDebug()) {
                this.logger.log('-----------------------------------------------')
                this.logger.log('Start block of ' + toBlock)
                this.logger.log('-----------------------------------------------')
            }
            let logs = []
            try {
                logs = await this.etherScanService
                    .getTransactionsByBlock(job.data.contracts, toBlock)
                if (Helper.isDebug()) {
                    this.logger.log('-----------------------------------------------')
                    this.logger.log(logs.length + ' logs are found :)')
                    this.logger.log('-----------------------------------------------')
                }
                if (logs.length > 0) {
                    emptyDeepLogsCounter = 0;
                    for (const log of logs) {
                        // totalOfLogs.push(log)
                        this.queueTransactions.add(EtherCollectTransactionFromLogsJobName, {
                            log
                        }).then()
                    }
                    toBlock = toBlock - 10000
                    if (Helper.isDebug()) {
                        this.logger.log('-----------------------------------------------')
                        this.logger.log('Number of calls for ' + job.data.contracts.length + ' contract: ' + this.etherScanService.getNumberOfCall())
                        this.logger.log('Next target: ' + toBlock)
                        this.logger.log('-----------------------------------------------')
                    }
                } else {
                    emptyDeepLogsCounter++;
                    if (emptyDeepLogsCounter > 5)
                        toBlock = null
                    else
                        toBlock = toBlock - 10000
                }
            } catch (err) {
                this.logger.warn('**************************')
                this.logger.warn(err)
                await DelayTool.sleep(5000)
            }
        }
        while (toBlock !== null)
        //
        // for (const log of totalOfLogs) {
        //     await this.queueTransactions.add(EtherCollectTransactionFromLogsJobName, {
        //         log
        //     })
        // }
        await job.data.contracts.forEach((contract) => {
            this.collectionRepository.transactionHistoryUpdated(contract.address[0]);
        })

    }
}
