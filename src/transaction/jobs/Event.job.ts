import {InjectQueue, Process, Processor} from "@nestjs/bull";
import {Job, Queue} from "bull";
import {OpenSeaTool} from "../tools/opensea.tool";
import {Logger} from "@nestjs/common";
import {DelayTool} from "../tools/delay.tool";
import {EventType} from "../transaction.enum";
import {Web3Tool} from "../tools/web3.tool";
import {EventCreateDTO} from "../dto/EventCreate.dto";
import {EventRepository} from "../repository/Event.repository";
import {TransactionService} from "../transaction.service";

@Processor('event-queue')
export class EventJob {

    private readonly logger: Logger = new Logger();

    constructor(
        private eventRepository: EventRepository,
        @InjectQueue('event-queue') private queue: Queue,
        private service: TransactionService
    ) {

    }

    @Process('collect-job')
    async readCollectJob(job: Job<any>) {
        let occurred_after = new Date(job.data.occurred_after)
        let occurred_before = new Date(job.data.occurred_before)
        let throttledCounter = 0;
        let response = await OpenSeaTool.getAll(job.data.type, occurred_after.getTime(), occurred_before.getTime(), job.data.limit, job.data.page * 50)
        while (response.data.detail && response.data.detail === 'Request was throttled.') {
            throttledCounter++;
            response = await OpenSeaTool.getAll(EventType.created, occurred_after.getTime(), occurred_before.getTime(), job.data.limit, job.data.page * 50)
            await DelayTool.sleep(15000);
            if (throttledCounter >= 3) {
                await this.queue.add('collect-job', {
                    type: job.data.type,
                    occurred_after: job.data.occurred_after,
                    occurred_before: job.data.occurred_before,
                    limit: job.data.limit,
                    page: (job.data.page + 1),
                });
                return
            }
        }
        if (response.data.asset_events && response.data.asset_events.length > 0) {
            response.data.asset_events.forEach((event: any) => {
                const timestamp = (event.transaction !== null) ? event.transaction.timestamp : event.created_date;
                let price = null;
                switch (job.data.type) {
                    case EventType.created:
                        price = parseFloat(Web3Tool.getUtils().fromWei((event.starting_price).toString(), "ether"));
                        break;
                    case EventType.successful:
                    case EventType.transfer:
                        price = (event.total_price !== null) ? parseFloat(Web3Tool.getUtils().fromWei((event.total_price).toString(), "ether")) : 0;
                        break;
                }


                // @ts-ignore
                let data: EventCreateDTO = {
                    name: event.asset.collection.name.toString(),
                    slug: event.collection_slug.toString(),
                    type: EventType.created.toString(),
                    tag: event.collection_slug.toString() + "-" + job.data.occurred_before.toISOString().substring(0, 10), // occurred_before = now
                    month: timestamp.substring(0, 7),
                    unit_price: price,
                    from: (event.transaction !== null) ? event.transaction.from_account.address : null,
                    to: (event.transaction !== null) ? event.transaction.to_account.address : null,
                    quantity: parseFloat(event.quantity),
                    action_date: timestamp,
                }
                this.eventRepository.create(data);
                this.service.detectCollections(event.collection_slug.toString(), event.asset.collection.name.toString())
            });
            await this.queue.add('collect-job', {
                type: job.data.type,
                occurred_after: job.data.occurred_after,
                occurred_before: job.data.occurred_before,
                limit: job.data.limit,
                page: (job.data.page + 1),
            });
        }
    }
}
