export interface PriceDifferenceCreateDTO {
    name: string
    slug: string
    token_id: string
    asset_name: string
    first_month_price: string
    end_month_price: string
    difference: number
    tag: string
}
