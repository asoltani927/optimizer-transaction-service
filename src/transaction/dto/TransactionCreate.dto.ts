
export interface TransactionCreateDTO {

    slug: string;

    contract_address: string;

    blockHash: string;

    blockNumber: string;

    gasUsed: string;

    cumulativeGasUsed: string;

    effectiveGasPrice: string;

    from: string;

    logs: Array<any>;

    logsBloom: string;

    to: string;

    transactionIndex: string;

    maxFeePerGas: string;

    maxPriorityFeePerGas: string;

    hash: string;
   
    nonce: string;

    accessList: Array<any>;

    chainId: string;

    value: string;

    cost: string;

    input: string;

    status: string;

    type: string;

    timestamp: string;

    created_time: Date;
}
