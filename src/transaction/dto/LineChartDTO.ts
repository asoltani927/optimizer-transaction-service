export interface LineChartDTO{
    name: string
    values: number[]
    labels:  string[]
}