import { Overtime } from '../transaction.enum';

export interface ChartFluctuationQueryDto {
  overtime: Overtime;
  slug: string;
}
