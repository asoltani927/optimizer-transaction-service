export interface TransactionCreateDTO {
    name: string
    slug: string
    type: string
    count: number
    avg: number
    tag: string
}
