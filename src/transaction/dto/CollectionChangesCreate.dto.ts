export interface CollectionChangesCreateDTO {
    name: string
    slug: string
    average: string
    tag: string
}
