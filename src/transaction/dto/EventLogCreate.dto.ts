
export interface EventLogCreateDTO {

    block_number: string;

    address: Array<string>;

    topics: Array<string>;

    content: Array<any>;
}
