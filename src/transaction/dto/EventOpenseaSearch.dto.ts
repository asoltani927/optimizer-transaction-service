import { EventType } from "../transaction.enum";

export interface EventOpenseaSearchDTO {
    slug: string, 
    type: EventType, 
    occurred_after: Number 
    occurred_before: Number 
}