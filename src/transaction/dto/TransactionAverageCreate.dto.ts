export interface TransactionAverageCreateDTO{
    name: string
    slug: string
    average: number,
    started_at: string,
    ended_at: string,
    type: string,
    during_days: number,
    transaction_count: number,
    data_count: number,
}
