export interface PriceChangesDto {
    name: string
    slug: string
    token_id: string
    asset_name: string
    average: number
    type: string
    started_at: string
    ended_at: string
    during_days: number
    data_count: number
}
