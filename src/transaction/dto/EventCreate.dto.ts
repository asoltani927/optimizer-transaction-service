export interface EventCreateDTO {
    name: string
    slug: string
    token_id: string
    asset_name: string
    type: string
    unit_price: number
    quantity: number
    from: string
    to: string
    tag: string
    month: string
    action_date: string
}
