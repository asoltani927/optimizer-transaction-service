export enum EventType{
    created = "created",
    successful = "successful",
    sale = "sale",
    transfer = "transfer",
}

export enum Overtime {
    weekly = 'Weekly',
    monthly = 'Monthly',
    sixMonth = 'SixMonth',
    threeMonth = 'ThreeMonth',
}
