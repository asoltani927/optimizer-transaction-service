/* eslint-disable prettier/prettier */
import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";

import {Seeder, DataFactory} from "nestjs-seeder";
import {EventType} from "../transaction.enum";
import {PricesChange} from "../schema/price.change.schema";

@Injectable()
export class PriceChangeSeeder implements Seeder {
    constructor(@InjectModel(PricesChange.name) private readonly model: Model<PricesChange>) {
    }

    async seed(): Promise<any> {
        // Generate 10 users.
        let start = new Date('2021-04-01T00:00:00');
        let end = new Date('2021-04-03T00:00:00');
        do {
            start.setDate(start.getDate() + 2)
            end.setDate(end.getDate() + 2)
            const data = DataFactory.createForClass(PricesChange).generate(1, {
                date: end,
                start: start,
                end: end,
                type: EventType.created,
                // eslint-disable-next-line prettier/prettier
                slug: 'roguesocietybot'
            });
            await this.model.insertMany(data);
            const data2 = DataFactory.createForClass(PricesChange).generate(1, {
                date: end,
                start: start,
                end: end,
                type: EventType.created,
                // eslint-disable-next-line prettier/prettier
                slug: 'ethereans-official'
            });
            await this.model.insertMany(data2);
        }
        while ((start.getTime() <= Date.now()))
    }

    async drop(): Promise<any> {
        return this.model.deleteMany({});
    }
}
