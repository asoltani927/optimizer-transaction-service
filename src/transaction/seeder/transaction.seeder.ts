/* eslint-disable prettier/prettier */
import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";

import {Seeder, DataFactory} from "nestjs-seeder";
import {EventType} from "../transaction.enum";
import {Transaction} from "../schema/transaction.schema";

@Injectable()
export class TransactionSeeder implements Seeder {
    constructor(@InjectModel(Transaction.name) private readonly model: Model<Transaction>) {
    }

    async seed(): Promise<any> {
        // Generate 10 users.
        let date = new Date('2021-04-01T00:00:00');
        do {
            date.setDate(date.getDate() + 1)
            const data = DataFactory.createForClass(Transaction).generate(1, {
                date: date,
                type: EventType.successful,
                // eslint-disable-next-line prettier/prettier
                slug: 'roguesocietybot'
            });

            await this.model.insertMany(data).then(console.log);
            const data2 = DataFactory.createForClass(Transaction).generate(1, {
                date: date,
                type: EventType.successful,
                // eslint-disable-next-line prettier/prettier
                slug: 'ethereans-official'
            });
            await this.model.insertMany(data2);
        }
        while ((date.getTime() <= Date.now()))
    }

    async drop(): Promise<any> {
        return this.model.deleteMany({});
    }
}
