/* eslint-disable prettier/prettier */
import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";

import {Seeder, DataFactory} from "nestjs-seeder";
import {Event} from "../schema/event.schema";
import {EventType} from "../transaction.enum";

@Injectable()
export class EventSeeder implements Seeder {
    constructor(@InjectModel(Event.name) private readonly model: Model<Event>) {
    }

    async seed(): Promise<any> {
        // Generate 10 users.
        let date = new Date('2021-04-01T00:00:00');
        do {
            date.setDate(date.getDate() + 1)
            let collection = ['roguesocietybot', 'ethereans-official'];
            collection.forEach((item) => {

                const data = DataFactory.createForClass(Event).generate(10, {
                    date: date,
                    type: EventType.sale,
                    // eslint-disable-next-line prettier/prettier
                    slug: item
                });

                const data2 = DataFactory.createForClass(Event).generate(5, {
                    date: date,
                    type: EventType.created,
                    // eslint-disable-next-line prettier/prettier
                    slug: item
                });
                const data3 = DataFactory.createForClass(Event).generate(8, {
                    date: date,
                    type: EventType.successful,
                    // eslint-disable-next-line prettier/prettier
                    slug:item
                });
                const data4 = DataFactory.createForClass(Event).generate(4, {
                    date: date,
                    type: EventType.transfer,
                    // eslint-disable-next-line prettier/prettier
                    slug:item
                });
                this.model.insertMany(data);
                this.model.insertMany(data2);
                this.model.insertMany(data3);
                this.model.insertMany(data4);
            })



        }
        while ((date.getTime() <= Date.now()))
    }

    async drop(): Promise<any> {
        return this.model.deleteMany({});
    }
}
