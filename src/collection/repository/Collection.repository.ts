import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {Collection, CollectionDocument} from '../schema/collection.schema';

@Injectable()
export class CollectionRepository {
    constructor(
        @InjectModel(Collection.name)
        private model: Model<CollectionDocument>,
    ) {
    }

    async update(slug, data) {
        await this.model.updateOne({
            slug: slug
        }, {$set: {...data}})
    }

    async aggregate(aggregate) {
        return this.model.aggregate(aggregate);
    }

    async findAll(): Promise<Collection[]> {
        return this.model.find();
    }

    async findBySlug(slug): Promise<Collection> {
        return this.model.findOne({slug: slug});
    }

    async findOneByAddress(address): Promise<Collection> {
        return this.model.findOne({
            contract_address: {$in: [address.toLowerCase()]}
        })
    }

    async transactionHistoryUpdated(address: string) {
        return this.model.updateOne({
            contract_address: {$in: [address.toLowerCase()]}
        }, {
            $set: {
                transaction_history: Date.now()
            }
        })
    }
}
