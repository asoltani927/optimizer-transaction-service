import {Module} from '@nestjs/common';
import {CollectionService} from './collection.service';
import {CollectionRepository} from './repository/Collection.repository';
import {MongooseModule} from '@nestjs/mongoose';
import {Collection, CollectionSchema} from './schema/collection.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Collection.name,
                schema: CollectionSchema,
            },
        ]),
    ],
    providers: [
        CollectionRepository,
        CollectionService,
    ],
    exports: [
        CollectionRepository,
        CollectionService,
    ]
})
export class CollectionModule {
}
