import {Injectable} from '@nestjs/common';
import {CollectionRepository} from './repository/Collection.repository';

@Injectable()
export class CollectionService {


    constructor(
        private readonly repository: CollectionRepository,
    ) {
    }

    async getCollectionUnRecovered() {
        return await this.repository.aggregate(
            [
                {
                    $match: {
                        transaction_history: undefined
                    }
                },
                // {
                //     $limit: 350
                // }
            ]
        );
    }

    async getCollectionBySlug(slug) {
        return this.repository.findBySlug(slug);
    }
}
