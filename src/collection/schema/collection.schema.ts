import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document} from 'mongoose';

export type CollectionDocument = Collection & Document;

@Schema()
export class Collection {
    @Prop({required: true})
    name: string;

    @Prop()
    slug: string;

    @Prop()
    contract_address: Array<string>;

    @Prop({default: false})
    check_again: boolean;

    @Prop({default: null})
    transaction_history: Date;

    @Prop({default: null})
    topics: Array<string>;

    @Prop({default: Date.now})
    created_at: Date;

    @Prop({default: Date.now})
    updated_at: Date;
}

export const CollectionSchema = SchemaFactory.createForClass(Collection);
