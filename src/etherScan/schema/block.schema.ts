import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document} from 'mongoose';

export type BlockDocument = Block & Document;

@Schema()
export class Block {
    @Prop({required: true})
    number: string

    @Prop({required: true})
    timestamp: string

    @Prop({default: Date.now})
    created_at: Date;

    @Prop({default: Date.now})
    updated_at: Date;
}

export const BlockSchema = SchemaFactory.createForClass(Block);
