export interface ContractInterface {
    address: Array<string>;
    slug: string;
    topics: Array<string>
}