import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Block, BlockDocument} from "../schema/block.schema";
import {BlockCreateDto} from "../dto/block.create.dto";

@Injectable()
export class BlockRepository {

    constructor(@InjectModel(Block.name) private model: Model<BlockDocument>) {
    }

    async create(dto: BlockCreateDto): Promise<Block> {
        let model = new this.model(dto)
        return await model.save();
    }

    async find(number): Promise<Block> {
        return this.model.findOne({
            number: number
        })
    }
}
