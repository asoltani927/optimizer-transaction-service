import QuickNodeConfig from "../config/quickNode.config";
import {Injectable, Logger} from "@nestjs/common";
import {DelayTool} from "../../transaction/tools/delay.tool";

@Injectable()
export class QuickNodeProvider {
    protected web3 = null
    protected counter = 0
    protected cooling = false

    private readonly logger: Logger = new Logger();

    constructor(private readonly config: QuickNodeConfig) {
    }

    protected initialize() {
        const Web3 = require('web3')
        if (this.web3 === null || !this.web3.currentProvider.isConnected)
            this.web3 = new Web3(new Web3.providers.HttpProvider(this.config.httpEndPoint))
        // this.web3 = new Web3(new Web3.providers.WebsocketProvider(this.config.wssEndPoint, {
        //     timeout: ,
        // clientConfig: {
        //     maxReceivedFrameSize: 1000000000,
        //     maxReceivedMessageSize: 1000000000,
        // }
        // }))
        // if (this.web3 === null) {
        //     switch (this.config.mode) {
        //         case 'http':
        //             this.web3 = new Web3(new Web3.providers.HttpProvider(this.config.httpEndPoint))
        //             break;
        //
        //         case 'wss':
        //             this.web3 = new Web3(new Web3.providers.WebsocketProvider(this.config.wssEndPoint))
        //             break;
        //
        //         default:
        //             throw 'Web3 Mode is not exists'
        //     }
        // }
    }

    protected prepare() {
        this.initialize();
    }

    public async eth() {
        while (this.cooling) {
            // this.logger.log('------- Quick Node Sleeping ---------- ')
            await DelayTool.sleep(60000);
            this.cooling = false
        }
        this.prepare();
        // this.counter++
        return this.web3.eth;
    }

    public utils() {
        this.prepare();
        return this.web3.utils;
    }

    public get() {
        this.prepare();
        return this.web3;
    }

    public getCounter() {
        return this.counter
    }

    public sleep() {
        this.cooling = true;
    }
}