import {Injectable, Logger} from '@nestjs/common';
import {QuickNodeProvider} from "./provider/quickNodeProvider";
import {ContractInterface} from "./interfaces/contract.interface";
import {BlockRepository} from "./repository/Block.repository";

@Injectable()
export class EtherScanService {

    private readonly logger: Logger = new Logger();

    constructor(
        private readonly provider: QuickNodeProvider,
        private readonly blockRepository: BlockRepository
    ) {
    }

    async getLastBlock() {
        return await (await this.provider.eth()).getBlockNumber()
    }

    async getTransactionsByBlock(contracts, toBlock) {
        let fromBlockNumber = toBlock - 9999;
        return this.getCollectionPastLogs(fromBlockNumber, toBlock, contracts)
    }

    async getPastLogs(fromBlock, ToBlock, contract: ContractInterface) {
        return await (await this.provider.eth()).getPastLogs({
            fromBlock: fromBlock,
            toBlock: ToBlock,
            address: contract.address,
            topics: contract.topics,
        })
    }

    async getCollectionPastLogs(fromBlock, ToBlock, contracts: Array<ContractInterface>) {

        let addresses = [];
        let topics = [];
        contracts.forEach(item => {
            item.address.forEach(contract => {
                addresses.push(contract)
            })
            item.topics.forEach(topic => {
                if (topics.indexOf(topic) === -1)
                    topics.push(topic)
            })
        })
        return  await (await this.provider.eth()).getPastLogs({
            fromBlock: fromBlock,
            toBlock: ToBlock,
            address: addresses,
            topics: topics
        })
    }

    async getTransactionByHash(hash) {
        return  await (await this.provider.eth()).getTransaction(hash);
    }

    async getTransactionReceiptByHash(hash) {
        return  await (await this.provider.eth()).getTransactionReceipt(hash);
    }

    async decodeTransactionInput(hash) {
        return  await (await this.provider.eth()).getTransactionReceipt(hash);
    }

    async getBlockTimestamp(blockNumber) {
        let model = await this.blockRepository.find(blockNumber.toString())
        if (model) {
            return model.timestamp
        } else {
            let block = await (await this.provider.eth()).getBlock(blockNumber);
            await this.blockRepository.create({
                number: blockNumber,
                timestamp: ((block.timestamp) ? block.timestamp.toString() : null)
            })
        }
    }

    getNumberOfCall() {
        return this.provider.getCounter()
    }

    async parseTransactionValue(value) {
        return this.provider.utils().fromWei(value, 'ether');
    }

    public sleep() {
        this.provider.sleep();
    }
}
