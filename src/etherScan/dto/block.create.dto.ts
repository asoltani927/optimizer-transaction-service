export class BlockCreateDto {
    number: string
    timestamp: string
}