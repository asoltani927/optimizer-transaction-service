import {Module} from '@nestjs/common';
import {EtherScanService} from './etherScan.service';
import QuickNodeConfig from "./config/quickNode.config";
import {QuickNodeProvider} from "./provider/quickNodeProvider";
import {MongooseModule} from "@nestjs/mongoose";
import {Block, BlockSchema} from "./schema/block.schema";
import {BlockRepository} from "./repository/Block.repository";

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Block.name,
                schema: BlockSchema,
            },
        ]),
    ],
    providers: [
        EtherScanService,
        QuickNodeConfig,
        QuickNodeProvider,

        BlockRepository,
    ],
    exports: [
        EtherScanService,
    ]
})
export class EtherScanModule {
}
