import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export default class QuickNodeConfig {
    constructor(private configService: ConfigService) {}

    get wssEndPoint(): string {
        return this.configService.get<string>('WEB3_WSS_HOST') || null;
    }

    get httpEndPoint(): string {
        return this.configService.get<string>('WEB3_HTTP_HOST') || null;
    }

    get mode(): string {
        return 'wss';
        return this.configService.get<string>('WEB3_MODE') || 'http';
    }
}
