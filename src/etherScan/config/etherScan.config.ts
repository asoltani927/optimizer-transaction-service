import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export default class EtherScanConfig {
    constructor(private configService: ConfigService) {}

    get endpoint(): string {
        return this.configService.get<string>('ETH_SCAN_API_KEY') || null;
    }
}
