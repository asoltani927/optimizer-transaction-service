import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export default class RedisConfig {
    constructor(private configService: ConfigService) {}
}
