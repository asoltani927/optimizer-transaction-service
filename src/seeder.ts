import {seeder} from "nestjs-seeder";
import {MongooseModule} from "@nestjs/mongoose";
import {MONGO_CONNECTION} from "./app.properties";
import {Event, EventSchema} from "./transaction/schema/event.schema";
import {Transaction, TransactionSchema} from "./transaction/schema/transaction.schema";
import {TransactionAverages, TransactionAveragesSchema} from "./transaction/schema/transaction.averages.schema";
import {TransactionAveragesSeeder} from "./transaction/seeder/transaction.averages.seeder";
import {EventSeeder} from "./transaction/seeder/event.seeder";
import {PriceChangeSeeder} from "./transaction/seeder/price.change.seeder";
import {TransactionSeeder} from "./transaction/seeder/transaction.seeder";
import {PricesChange, PricesChangeSchema} from "./transaction/schema/price.change.schema";

seeder({
    imports: [
        MongooseModule.forRoot(MONGO_CONNECTION),
        MongooseModule.forFeature([
            {
                name: Event.name,
                schema: EventSchema,
            },
            {
                name: Transaction.name,
                schema: TransactionSchema,
            },
            {
                name: TransactionAverages.name,
                schema: TransactionAveragesSchema,
            },
            {
                name: PricesChange.name,
                schema: PricesChangeSchema,
            },
        ]),
    ]
}).run([
    EventSeeder,
    TransactionAveragesSeeder,
    PriceChangeSeeder,
    TransactionSeeder
]);
