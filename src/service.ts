import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {MicroserviceOptions, Transport} from "@nestjs/microservices";
import AppConfig from "./config/app.config";

async function bootstrap() {
    const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
        transport: Transport.REDIS,
        options: {
            url: process.env.REDIS_URL,
            // queue: 'main_queue',
            // queueOptions: {
            // durable: false
            // }
        }
    });

    //Get app config
    const appConfig: AppConfig = app.get(AppConfig);

    await app.listen();

    return (
        (appConfig.IsDebugMode ? 'Implementation' : 'Published')
    );
}

bootstrap().then((r) => console.log('Application is ready on ' + r));
