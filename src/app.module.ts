import {HttpModule} from '@nestjs/axios';
import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ScheduleModule} from '@nestjs/schedule';
import {MONGO_CONNECTION} from './app.properties';
import {TransactionModule} from './transaction/transaction.module';
import {ConfigModule} from '@nestjs/config';
import {BullModule} from "@nestjs/bull";
import {EtherScanModule} from './etherScan/etherScan.module';
import {CollectionModule} from "./collection/collection.module";
import AppConfig from "./config/app.config";
import QuickNodeConfig from "./etherScan/config/quickNode.config";

// @ts-ignore
@Module({
    imports: [
        ScheduleModule.forRoot(),
        ConfigModule.forRoot({isGlobal: true}),
        HttpModule.register({
            timeout: 5000,
            maxRedirects: 3
        }),
        BullModule.forRoot({
            redis: {
                host: 'localhost',
                port: parseInt(process.env.REDIS_PORT),
            },
        }),
        MongooseModule.forRoot(MONGO_CONNECTION),

        CollectionModule,
        TransactionModule,
        EtherScanModule,
    ],
    providers: [
        AppConfig, QuickNodeConfig
    ]
})
export class AppModule {
}
